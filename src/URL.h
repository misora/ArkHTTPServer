#pragma once
#include <string>

namespace Ark
{
	class URL
	{
	public:
		URL();
		URL(const std::string& url);
		~URL();

	public:
		bool IsValid() const;
		const std::string& GetURL() const;

	private:
		std::string _url;

		bool _parsed = false;
		std::string _schema;
		std::string _host;
		std::string _path;
		std::string _query;
		std::string _fragment;
	};
}