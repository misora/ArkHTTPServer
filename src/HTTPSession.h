#pragma once


namespace Ark
{
	class HTTPSession
	{
	public:
		HTTPSession();
		~HTTPSession();

	private:
		HTTPSession(const HTTPSession&) = delete;
		void operator = (const HTTPSession&) = delete;

	private:

	};

}