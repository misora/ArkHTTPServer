#include "HTTPServer.h"


namespace Ark
{
	using namespace asio;
	using namespace asio::ip;

	HTTPServer::HTTPServer()
	{
		
	}

	HTTPServer::~HTTPServer()
	{

	}
	//bool HTTPServer::Listen(int fd)
	//{
	//	asio::io_service loop;
	//	
	//	asio::io_context io;

	//	//ip::tcp::endpoint(tcp::v4(), )
	//	//asio::ip::address addr;
	//	//addr.from_string()
	//	
	//	//_acceptor.reset(new asio::ip::tcp::acceptor());
	//	return false;
	//}

	bool HTTPServer::Listen(int port)
	{
		_acceptor.reset(new tcp::acceptor(_ioContext, tcp::endpoint(tcp::v4(), (unsigned short)port)));

		_Accept();
		return true;
	}


	void HTTPServer::_Accept()
	{
		_acceptor->async_accept([this](std::error_code ec, tcp::socket socket) {
			if (!ec) {
				_OnAccept(std::move(socket));
			}
			_Accept();
		});
	}


	void HTTPServer::_OnAccept(asio::ip::tcp::socket&& socket)
	{
		
	}



}