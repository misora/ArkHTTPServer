#pragma once
#include "asio.hpp"

namespace Ark
{
	class HTTPConnection
	{
	public:
		HTTPConnection();
		~HTTPConnection();

	private:
		HTTPConnection(const HTTPConnection&) = delete;
		void operator = (const HTTPConnection&) = delete;

	public:
		void Attach(std::shared_ptr<asio::ip::tcp::socket> socket);
		

	private:
		std::string _host;	
		asio::ip::tcp::endpoint _addr;
		std::shared_ptr<asio::ip::tcp::socket> _socket;
	};
}