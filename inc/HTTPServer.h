#pragma once
#include <memory>

#include "asio.hpp"
#include "HTTPRequest.h"
#include "HTTPResponse.h"
#include "HTTPSession.h"

namespace Ark
{
    class HTTPServer
    {
    public:
		HTTPServer();
		~HTTPServer();
	private:
		HTTPServer(const HTTPServer&) = delete;
		void operator = (const HTTPServer&) = delete;

	public:
		typedef std::function<void(std::shared_ptr<HTTPSession>)> OnRequestFunction;
		//bool Listen(int fd);
		bool Listen(int port);

	private:
		void _Accept();
		void _OnAccept(asio::ip::tcp::socket&& socket);

	private:
		std::unique_ptr<asio::ip::tcp::acceptor> _acceptor;
		asio::io_context _ioContext;
    };
}