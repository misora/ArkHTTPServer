#pragma once

namespace Ark
{
	class HTTPResponse
	{
	public:
		HTTPResponse();
		~HTTPResponse();

	private:


	public:
		void Write();
		void Finish();

	private:

	};
}